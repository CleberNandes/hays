import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompraComponent } from './compra.component';
import { Routes, RouterModule } from '@angular/router';
import { ResumoCarrinhoModule } from 'src/app/components/resumo-carrinho/resumo-carrinho.module';

const routes: Routes = [
  {
    path: '',
    component: CompraComponent,
    data: { title: 'Compra Demo Shop' },
    children: [
      {
        path: '',
        redirectTo: 'pagamento',
        pathMatch: 'full'
      },
      {
        path: 'pagamento',
        loadChildren: '../pagamento/pagamento.module#PagamentoModule'
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    ResumoCarrinhoModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CompraComponent
  ]
})
export class CompraModule { }
