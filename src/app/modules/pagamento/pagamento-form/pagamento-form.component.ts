import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Pagamento } from 'src/app/core/models/Pagamento';
import { CartaoService } from 'src/app/core/utils/cartao.service';
import { PagamentoService } from 'src/app/core/services/pagamento.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'hays-pagamento-form',
  templateUrl: './pagamento-form.component.html',
  styleUrls: ['./pagamento-form.component.scss']
})
export class PagamentoFormComponent implements OnInit {
  public focusSubject = this.cartaoService.focusSubject;
  public flagSubject = this.cartaoService.flagSubject;
  public pagamentoForm: FormGroup;
  @Input() public pagamento: Pagamento;
  public parcelas: Array<any> = [];

  constructor(
    private cartaoService: CartaoService,
    private formBuilder: FormBuilder,
    private pagamentoService: PagamentoService
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getParcelas();
  }

  public save() {
    console.log(this.pagamentoForm.value);

    // this.pagamentoService.save(this.pagamentoForm.value)
    //   .pipe(take(1)) // unsubscribe automatico
    //   .subscribe(res => console.log(res));
  }

  public checkCreditCardFlag() {
    if (this.pagamento.numeroCartao) {
      const flags = ['3', '4', '5', '6'];
      const firstNumber = this.pagamento.numeroCartao.split('')[0];
      if (this.pagamento.numeroCartao.length === 1) {
        this.setFlag(firstNumber);
      } else if (!flags.includes(firstNumber) || this.pagamento.numeroCartao.length !== 19) {
        this.pagamentoForm.get('numeroCartao').setErrors({'Mask error': true});
      }
    }
  }

  private setFlag(value) {
    const flags = [
      {id: '3', kind: 'american.png'},
      {id: '4', kind: 'visa.png'},
      {id: '5', kind: 'mastercard.png'},
      {id: '6', kind: 'discovery.png'}
    ];
    const flag = flags.find(item => item.id === value);
    if (!flag) {
      this.flagSubject.next(null);
    } else {
      this.flagSubject.next(flag.kind);
    }
  }

  public checkValidade() {
    if (this.pagamento.validade && this.pagamento.validade.length === 5) {
      const arrayDate = this.pagamento.validade.split('/');
      const today = new Date();
      const cardDate = new Date();
      cardDate.setFullYear(parseInt('20' + arrayDate[1], 10));
      cardDate.setMonth(parseInt(arrayDate[0], 10) - 1);
      if (today >= cardDate) {
        this.pagamentoForm.get('validade').setErrors({'Mask error': true});
      }
    }
  }

  private getParcelas() {
    for (let i = 1; i < 13; i++) {
      const valor = 12000 / i;
      this.parcelas.push({
        id: i, valor, label: i + 'x R$' + valor.toFixed(2) + ' sem juros'
      });
    }
  }

  private buildForm() {
    this.pagamentoForm = this.formBuilder.group({
      numeroCartao: [null, [
        Validators.required
      ]],
      nome: [null, [
        Validators.required,
        // Validators.pattern(/^[A-Z][a-z]([-']?[A-Z][a-z]+)*( [A-Z][a-z]([-']?[A-Z][a-z]+)*)+$/)
        Validators.pattern(/[A-Z][a-z]* [A-Z][a-z]*/)

      ]],
      validade: [null, [
        Validators.required,
        Validators.minLength(5),
      ]],
      cvv: [null, [
        Validators.required,
        Validators.minLength(3)
      ]],
      parcelas: [null, [
        Validators.required
      ]]
    });
  }
}
