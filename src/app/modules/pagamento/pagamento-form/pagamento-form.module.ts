import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagamentoFormComponent } from './pagamento-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InputTextModule } from 'src/app/components/input-text/input-text.module';
import { SelectModule } from 'src/app/components/select/select.module';
import { PagamentoService } from 'src/app/core/services/pagamento.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    InputTextModule,
    SelectModule
  ],
  declarations: [ PagamentoFormComponent ],
  exports: [ PagamentoFormComponent ],
  providers: [
    PagamentoService
  ]
})
export class PagamentoFormModule { }
