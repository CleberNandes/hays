import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { Pagamento } from 'src/app/core/models/Pagamento';
import { InputTextComponent } from 'src/app/components/input-text/input-text.component';

@Component({
  templateUrl: './pagamento.component.html',
  styleUrls: ['./pagamento.component.scss']
})
export class PagamentoComponent implements OnInit {

  // Visa card numbers always begin with a 4
  // Mastercard numbers begin with a 5
  // Discovery card's number will begin with a 6
  // American Express card numbers always start with a 3

  public pagamento: Pagamento;

  constructor(

  ) { }

  ngOnInit() {

    this.pagamento = {
      numeroCartao: null,
      nome: null,
      validade: null,
      cvv: null,
      parcelas: null
    };
  }





}
