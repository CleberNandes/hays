import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PagamentoComponent } from './pagamento.component';
import { PagamentoService } from 'src/app/core/services/pagamento.service';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CartaoModule } from 'src/app/components/cartao/cartao.module';
import { PagamentoFormModule } from './pagamento-form/pagamento-form.module';
import { StepperModule } from 'src/app/components/stepper/stepper.module';

const routes: Routes = [
  {
    path: '',
    component: PagamentoComponent,
    data: { title: 'Compra Demo Shop' }
  }
];
@NgModule({
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    RouterModule.forChild(routes),
    // cartao
    CartaoModule,
    PagamentoFormModule,
    StepperModule
  ],
  declarations: [
    PagamentoComponent
  ]
})
export class PagamentoModule { }
