import { Component } from '@angular/core';

@Component({
  selector: 'hays-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'hays';
}
