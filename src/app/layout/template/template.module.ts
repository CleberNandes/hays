import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateComponent } from './template.component';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { ItensMenuComponent } from '../itens-menu/itens-menu.component';

const routes: Routes = [
  {
    path: '',
    component: TemplateComponent,
    data: { title: 'Demo Shop' },
    children: [
      {
        path: '',
        redirectTo: 'compra',
        pathMatch: 'full'
      },
      {
        path: 'compra',
        loadChildren: '../../modules/compra/compra.module#CompraModule'
      }
    ]
  }
];
@NgModule({
  // name	String	F-A Icons	No
  // size	String	lg, 2x, 3x, 4x, 5x	Yes
  // fixed	Boolean	true | false	Yes
  // animation	String	spin | pulse	Yes
  // rotate	Number | String	90 | 180 | 270 horizontal | vertical	Yes
  // inverse	Boolean	true | false
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    TemplateComponent,
    HeaderComponent,
    SidebarComponent,
    ItensMenuComponent
  ]
})
export class TemplateModule { }
