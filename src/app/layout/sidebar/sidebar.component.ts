import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hays-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public isShown = false;

  constructor() { }

  public ngOnInit() {
  }

  public toggle() {
    this.isShown = !this.isShown;
  }

}
