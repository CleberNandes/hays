import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectComponent } from './select.component';
import {MatSelectModule} from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { ValidMsgPipeModule } from 'src/app/core/pipe/valid-msg.pipe';


@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
    ReactiveFormsModule,
    ValidMsgPipeModule
  ],
  declarations: [ SelectComponent ],
  exports: [ SelectComponent ]
})
export class SelectModule { }
