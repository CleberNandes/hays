import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';
import { CustomValueAccessor } from '../../core/models/CustomValueAccessor';
import { ErrorMatcher } from 'src/app/core/models/ErrorMatcher';

@Component({
  selector: 'hays-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    }
  ]
})
export class SelectComponent extends CustomValueAccessor {
  public matcher = new ErrorMatcher();
  @Input() public label: string;
  @Input() public list: Array<any>;
  @Input() public disabled = false;
  @Input() public formControlName: string;
  @Input() public msg: string;
  @Input() control: FormControl;

  constructor(
  ) { super(); }

  public changeValue(evento) {

    this.onChange(evento.target.value);
    this.value = evento.target.value;
  }

}
