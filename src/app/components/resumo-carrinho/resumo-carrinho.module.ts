import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResumoCarrinhoComponent } from './resumo-carrinho.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ResumoCarrinhoComponent
  ],
  exports: [
    ResumoCarrinhoComponent
  ]
})
export class ResumoCarrinhoModule { }
