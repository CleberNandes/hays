import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Pagamento } from 'src/app/core/models/Pagamento';
import { CartaoService } from '../../core/utils/cartao.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'hays-cartao',
  templateUrl: './cartao.component.html',
  styleUrls: ['./cartao.component.scss']
})
export class CartaoComponent implements OnInit, OnDestroy {
  private focusSubject = this.cartaoService.focusSubject;
  @Input() public pagamento: Pagamento;
  private subscription: Subscription;
  public flag: string;


  constructor(private cartaoService: CartaoService) { }

  public ngOnInit() {
    this.subscription = this.focusSubject.subscribe(val => {
      this.flipCartao();
    });
    this.subscription.add(
      this.cartaoService.flagSubject.subscribe(
        (flag: string) => this.flag = flag
      )
    );
  }

  public flipCartao() {
    const cartao = document.querySelector('.cartao-inner') as HTMLDivElement;
    cartao.classList.toggle('cartao-rotate');
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
