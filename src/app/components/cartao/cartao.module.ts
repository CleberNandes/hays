import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartaoComponent } from './cartao.component';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,

    NgxMaskModule
  ],
  declarations: [
    CartaoComponent
  ],
  exports: [
    CartaoComponent
  ]
})
export class CartaoModule { }
