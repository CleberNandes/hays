import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextComponent } from './input-text.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {NgxMaskModule} from 'ngx-mask';
import { ValidMsgPipeModule } from 'src/app/core/pipe/valid-msg.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    NgxMaskModule,
    ReactiveFormsModule,
    ValidMsgPipeModule
  ],
  declarations: [ InputTextComponent ],
  exports: [
    InputTextComponent
  ]
})
export class InputTextModule { }
