import { Component, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';
import { CustomValueAccessor } from '../../core/models/CustomValueAccessor';
import { ErrorMatcher } from 'src/app/core/models/ErrorMatcher';

@Component({
  selector: 'hays-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: [ './input-text.component.scss' ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputTextComponent),
      multi: true
    }
  ]
})
export class InputTextComponent extends CustomValueAccessor {

  // 0	digits (like 0 to 9 numbers)
  // 9	digits (like 0 to 9 numbers), but optional
  // A	letters (uppercase or lowercase) and digits
  // S	only letters (uppercase or lowercase)
  @Output() inputFocus = new EventEmitter<boolean>();
  matcher = new ErrorMatcher();

  @Input() public label: string;
  @Input() public place: string;
  @Input() public disabled = false;
  @Input() public formControlName: string;
  @Input() public mask = '';
  @Input() public msg: string;
  @Input() public value: any;
  @Input() control: FormControl;

  constructor(
  ) { super(); }

  public changeValue(evento) {
    this.onChange(evento.target.value);
    this.value = evento.target.value;
  }

  public onFocus(value: boolean) {
    this.inputFocus.emit(value);
  }

}
