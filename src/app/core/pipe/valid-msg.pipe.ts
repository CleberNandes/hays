import { Pipe, PipeTransform, NgModule } from '@angular/core';
import { ValidationService } from '../validators/validation.msg';
import { FormControl } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Pipe({
  name: 'validMsgPipe'
})
export class ValidMsgPipe implements PipeTransform {

  transform(control: FormControl, type: string): any {

    for (const validationName in control.errors) {

      if (control.errors.hasOwnProperty(validationName) && control.touched) {
        return ValidationService.getValidatorErrorMsg(
          validationName,
          control.errors[validationName],
          type
        );
      }
    }
    return null;
  }
}
@NgModule({
  imports: [ CommonModule ],
  declarations: [ ValidMsgPipe ],
  exports: [ ValidMsgPipe ]
})
export class ValidMsgPipeModule { }
