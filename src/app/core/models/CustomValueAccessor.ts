import { AbstractControl, ControlValueAccessor, FormControl } from '@angular/forms';

export class CustomValueAccessor implements ControlValueAccessor {

  public value: any = null;
  public onChange: (_: any) => void;
  public onTouched: () => void;
  public disabled = false;

  public onBlur() {
    this.onTouched();
  }

  public writeValue(value: any): void {
    this.value = value;
  }

  public registerOnChange(fn: (value: any) => any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
