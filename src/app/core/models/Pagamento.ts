export interface Pagamento {
  numeroCartao: string;
  nome: string;
  validade: string;
  cvv: string;
  parcelas: string;
}
