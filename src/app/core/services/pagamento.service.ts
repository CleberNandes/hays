import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, ReplaySubject } from 'rxjs';
import { Pagamento } from '../models/Pagamento';

@Injectable()
export class PagamentoService {

  constructor(
    private http: HttpClient
  ) { }

  public save(pagamento: Pagamento): Observable<boolean> {
    return this.http.post('/pagar', pagamento)
      // tslint:disable-next-line: no-string-literal
      .pipe(res => res['save']);
  }

}
