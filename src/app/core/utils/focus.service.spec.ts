/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CartaoService } from './cartao.service';

describe('Service: Focus', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CartaoService]
    });
  });

  it('should ...', inject([CartaoService], (service: CartaoService) => {
    expect(service).toBeTruthy();
  }));
});
