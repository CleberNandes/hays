import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartaoService {
  public focusSubject = new ReplaySubject(1);
  public flagSubject = new ReplaySubject(1);

}
