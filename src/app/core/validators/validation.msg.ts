import { ValidationErrors } from '@angular/forms';

export class ValidationService {

  static getValidatorErrorMsg(
    validatorName: string,
    validatorValue: ValidationErrors,
    type: string,
  ) {
    const config = {
      numeroCartao: {
        required: 'Número de cartão inválido',
        'Mask error': 'Número de cartão inválido'
      },
      nome: {
        required: 'Insira seu nome completo',
        pattern: 'Insira seu nome completo'
      },
      validade: {
        required: 'Data inválida',
        'Mask error': 'Data inválida',
        pattern: 'Data inválida',
        minlength: 'Data inválida',
      },
      cvv: {
        required: 'Código inválido',
        'Mask error': 'Código inválido',
        minlength: 'Código inválido',
      },
      parcelas: {
        required: 'Insira o número de parcelas'
      },
    };
    return config[type][validatorName];
  }
}
