## Hays

Criando projeto para teste de Especialista Front end na Hays

### Tecnologias usadas no projeto
* grid personalizado
* Lazy-load
* ngx-mask
* Angular material

## Como rodar o Projeto
* ter o node instalado
* ter o git instalado
* clonar o projeto `git clone https://bitbucket.org/CleberNandes/hays.git`
* entrar na pasta do projeto `cd hays`
* `npm install`
* `ng serve`

Pronto, agora é só usar.

#### Algumas melhorias foram atendidas que não estavam sendo requeridas, como: 

* Melhoria no header da aplicação - sumindo no scroll down
* Girando cartão no mouse hover e no focus do campo cvv
* Validação do numero do cartão para aparecer a bandeira conforme o primeiro numero
* Validação na validade do cartão se for uma data do passado
* Sidebar com itens do nav header com animação

#### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

#### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

#### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

#### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

#### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

#### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
